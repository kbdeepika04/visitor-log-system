/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package in.ac.gpckasaragod.visitor.log.system.ui;

import in.ac.gpckasaragod.visitor.log.system.service.RoomDetailsService;
import java.awt.List;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import static java.lang.Character.getType;
import static java.lang.System.out;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;



/**
 *
 * @author student
 */
public class RoomDetailsForm extends javax.swing.JFrame {
    public RoomDetailsService  roomdetailsService =new RoomDetailsServiceImpl;

    

    /**
     * Creates new form RoomDetails
     */
    public RoomDetailsForm() {
    
        
    }

    private RoomDetailsForm(Type type) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
      
        
  
        

 
          
              
         
      
    /**Overridemodel
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblId = new javax.swing.JTable();
        btnAdd = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();

        tblId.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "ID", "TYPE", "RATE"
            }
        ));
        jScrollPane1.setViewportView(tblId);

        btnAdd.setText("ADD");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnEdit.setText("EDIT");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnDelete.setText("DELETE");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(84, 84, 84)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 534, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(125, 125, 125)
                        .addComponent(btnAdd)
                        .addGap(70, 70, 70)
                        .addComponent(btnEdit)
                        .addGap(60, 60, 60)
                        .addComponent(btnDelete)))
                .addContainerGap(224, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(50, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdd)
                    .addComponent(btnEdit)
                    .addComponent(btnDelete))
                .addGap(14, 14, 14))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        // TODO add your handling code here:
        int rowIndex =tblRoomDetails.convertRowIndexToModel(tblRoomDetails.getSelectRow());
        Integer id =(Integer) tblRoomDetails.getvalueAt(rowIndex,0);
        int result=JOptionPane.showConfirmDialog(RoomDetailsForm.this,"Are you sure to delete");
        if(result ==JOptionPane.YES_OPTION){
            String message =roomdetailsService.deleteRoomDetails(id);
            JOptionPane.showMessageDialog(RoomDetailsForm.this,message);
            loadTableValues();
        }
            
        
        
        
        
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
 // TODO add your handling code here:
        JFrame window =new JFrame();
        window.setVisible(true);
        RoomDetailForm roomdetailsForm =new RoomDetailForm();
        window.add(roomdetailsForm);
        window. pack();
        window.setLocationRelativeTo(null);
        window.addWindowListener(new WindowAdapter(){
             
        public void windowClosed(WindowEvent e){   
            loadTableValues();
        System.out.print("Window Closing");
        }
     
        public void windowClosing(WindowEvent e){
            super.windowClosing(e);
            System.out.print("Window Closing");
            loadTableValues();
        }

            private void loadTableValues() {
                throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
            
            List<RoomDetails> roomdetails =roomdetailsService.getAllRoomDetails();
        DefaultTableModel model =(DefaultTableModel)tblRoomDetail.getModel();
        model.setRowCount(0);
        for(Roomdetails roomdetails:roomdetails){
        model.addRow(new Object[]{roomdetails.getId(),roomdetails.getType(),roomdetails.getRate());
        
  
        
    }                                      Overridemodel//GEN-LAST:event_btnAddActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        // TODO add your handling code here:
        int rowIndex =tbl RoomDetails.convertRowIndexModel(tblRoomDetails.get selectedRow());
        Integer id =(Integer) tblroomdetails.getValueAt(rowIndex,0);
        RoomDetails roomdetails =roomdetailsService.readRoomDetails(type);
         JFrame window =new JFrame();
         window.setVisible(true);
         RoomDetailsForm roomdetailsForm = new RoomDetailsForm(type);
         window.add(RoomDetailsForm);
         window.setSize(600,600);
         window.pack();
         window.setLocationRelativeTo(null);
         window.addWindowListener(new WindowAdapter(){
          @Override
          public void windowClosed(WindowEvent e){
              loadTableValues();
              System.out.print("window closed");
          }
          @Override
          public void windowClosing(WindowEvent e){
           super.windowClosing(e);
           System.out.print("window closed");
           loadTableValues();
          }
         
    
     
         
             
         
         
         
        
        
        
        
        
    }//GEN-LAST:event_btnEditActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblId;
    // End of variables declaration//GEN-END:variables

   

            
      
    

        

