/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.visitor.log.system.ui.model.data;

/**
 *
 * @author student
 */
public class VisitorDetails {
    private Integer id;
    private String name;
    private String address;
    private String phoneNo;
    private Integer room_id;
    private String Idproof;
    private String booking_date;
    private Integer no_of_days;

    public VisitorDetails(Integer id, String name, String address, String phoneNo, Integer room_id, String Idproof, String booking_date, Integer no_of_days) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.phoneNo = phoneNo;
        this.room_id = room_id;
        this.Idproof = Idproof;
        this.booking_date = booking_date;
        this.no_of_days = no_of_days;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public Integer getRoom_id() {
        return room_id;
    }

    public void setRoom_id(Integer room_id) {
        this.room_id = room_id;
    }

    public String getIdproof() {
        return Idproof;
    }

    public void setIdproof(String Idproof) {
        this.Idproof = Idproof;
    }

    public String getBooking_date() {
        return booking_date;
    }

    public void setBooking_date(String booking_date) {
        this.booking_date = booking_date;
    }

    public Integer getNo_of_days() {
        return no_of_days;
    }

    public void setNo_of_days(Integer no_of_days) {
        this.no_of_days = no_of_days;
    }
}
