/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.visitor.log.system.service;

import in.ac.gpckasaragod.visitor.log.system.ui.VisitorDetailsForm;
import java.awt.List;
import java.sql.Date;

/**
 *
 * @author student
 */
public interface VisitorDetailsService {

    public static String save;
    public static String update;
    public String saveVisitorDetails(String name,String address,String phoneno,String idproof,String roomid,String no_of_days,Date booking_date);
    public VisitorDetailsForm readVisitorDetails(Integer id);
    public List<VisitorDetailsForm>getAllVisitorDetails();
    public String updateVisitorDetails(VisitorDetailsForm visitordetails);
    public String updateVisitorDetails(String name,String address,String phoneno,String idproof,String roomid,String no_of_days,Date booking_date);
    public String deleteVisitorDetails(Integer id);
}