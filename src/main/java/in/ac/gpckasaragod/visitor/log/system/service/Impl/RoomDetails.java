/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.visitor.log.system.service.Impl;

/**
 *
 * @author student
 */
class RoomDetails {
    

  
    private Integer id;
    private String type;
     private Float rate;

    public RoomDetails(Integer id, String type, Float rate) {
        this.id = id;
        this.type = type;
        this.rate = rate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }
 }
