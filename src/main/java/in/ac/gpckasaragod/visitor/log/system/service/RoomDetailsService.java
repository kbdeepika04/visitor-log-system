
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.visitor.log.system.service;

import in.ac.gpckasaragod.visitor.log.system.ui.VisitorDetailsForm;
import java.awt.List;
import java.sql.Date;

/**
 *
 * @author student
 */
public interface RoomDetailsService {
    

public String saveRoomDetails(String type,Float rate);

public RoomDetails readRoomDetails(Integer id);

    public String saveRoomDetails(String type,Float rate);
    public RoomDetails readRoomDetails(Integer id);
    public List<RoomDetails>getAllRoomDetails();
    public String updateVisitorDetails(RoomDetails roomdetails);
    
    public String deleteRoomDetails(Integer id);
}